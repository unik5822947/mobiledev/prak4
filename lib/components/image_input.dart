import 'dart:io';
import 'package:image_picker/image_picker.dart';

class ImageInput {
  final _picker = ImagePicker();

  Future<File?> getImage() async {
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      if (pickedFile != null) {
        return File(pickedFile.path);
      } else {
        print('No image selected.');
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
