import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseStorageService {
  static Future<String> uploadImage(File image) async {
    try {
      final String fileName = DateTime.now().toUtc().toIso8601String();
      final Reference firebaseStorageRef =
          FirebaseStorage.instance.ref().child('images/$fileName');
      await firebaseStorageRef.putFile(image);
      final String downloadURL = await firebaseStorageRef.getDownloadURL();
      return downloadURL;
    } catch (e) {
      print(e);
      return '';
    }
  }
}
